<?php

header('Content-Type: text/html; charset=utf-8');

/* Defs */

define("CALLBACK_URL", "http://test.local/wbwgs/");

define("AUTH_URL", "https://delta.authentica.webwings.dev/api/authorize");

define("TOKEN_URL", "https://delta.authentica.webwings.dev/api/token");

define("API_URL", "https://delta.authentica.webwings.dev/api");

define("CLIENT_ID", "51acaa8e650af0a1948877f6a749bef5");

define("CLIENT_SECRET", "5003a20d7f9e3dda6c92be527f08bf8846d824736c896323f266b05cf2308c4701abaca212ef4c198381c13a333def95b913d535dc310ced0d93f1e24de37639");

define("SCOPE", "default api");



$shopId = 16;

$url = AUTH_URL."?"
    ."response_type=code"
    ."&client_id=". urlencode(CLIENT_ID)
    ."&scope=". urlencode(SCOPE)
    ."&redirect_uri=". urlencode(CALLBACK_URL)
;

$code = $_REQUEST['code'];

if(!isset($accessToken)) {

    if(!$code) {

        header("Location: ".$url, true, 301); exit();

    } else {

        $response = getToken($code);
        $accessToken = $response['access_token'];
        $refreshToken = $response['refresh_token'];

        $product = array(
            "name" => "Test product",
            "declarationName" => "Test product declaration",
            "description" => 'test',
            "note" => 'test',
            "length" => 10,
            "width" => 10,
            "height" => 5,
            "weight" => 0.5,
            "skus" => array(
                "sku-0007-01-a"
            )
        );

        if(isset($accessToken) && !empty($accessToken)) {

            $productResponse = createProduct($accessToken, $product, $shopId);

            if(isset($productResponse['id'])) {

                echo 'Produkt ['.$productResponse['id'].'] vytvořen. ';

                $order = array(
                    "externalId" => "external-002",
                    "carrierId" => 8,
                    "branchId" => null,
                    "price" => "153.00",
                    "priceCurrency" => "CZK",
                    "cod" => false,
                    "codValue" => null,
                    "codValueCurrency" => null,
                    "vs" => null,
                    "companyName" => "Webwings s.r.o.",
                    "firstName" => null,
                    "lastName" => null,
                    "addressLine1" => "Marešova 14",
                    "addressLine2" => "2nd floor",
                    "addressLine3" => null,
                    "city" => "Brno",
                    "zip" => "602 00",
                    "country" => "CZ",
                    "state" => null,
                    "phone" => "+420808875329",
                    "email" => "developers@webwings.cz",
                    "processingDate" => "2023-01-24",
                    "items" =>array(
                        array(
                            "productId" => $productResponse['id'],
                            "amount" => 1
                        )
                    )
                );

                $orderResponse = createOrder($accessToken, $order, $shopId);

                if(isset($orderResponse['id'])) {

                    echo 'Objednávka ['.$orderResponse['id'].'] vytvořena. ';

                }

            }

        }

    }

}


function getToken($code){
 
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, TOKEN_URL);
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, array(
    	'code' => $code,
    	'client_id' => CLIENT_ID,
    	'client_secret' => CLIENT_SECRET,
    	'redirect_uri' => CALLBACK_URL,
    	'grant_type' => 'authorization_code'
    ));

    $response = curl_exec($ch);
    $err = curl_error($ch);

    curl_close($ch);
 
    if ($err) {

        echo "cURL Error #01 (Token): " . $err;

    } else {

        $response = json_decode($response, true);
        if(array_key_exists("access_token", $response)) return $response;
        if(array_key_exists("error", $response)) echo $response["error_description"];
        echo "cURL Error #02 (Token): Something went wrong! Please contact admin.";

    }
}

function createProduct($accessToken, $product, $shopId = 16) {
    $createProductApiUrl = API_URL.'/shop/'.$shopId.'/product';

    $postdata = json_encode($product);

    $header = array(
        'Authorization: Bearer '.$accessToken, 
        'Content-Type: application/json',
        //'Content-Length: ' . strlen($postdata)
    );
 
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $createProductApiUrl);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);

    $response = curl_exec($ch);
    $err = curl_error($ch);

    curl_close($ch);

    if ($err) {

        echo "cURL Error #01 (Product): " . $err;

    } else {

        $response = json_decode($response, true);
        if(array_key_exists("id", $response)) return $response;
        if(array_key_exists("error", $response)) echo $response["error_description"];
        echo "cURL Error #02 (Product): Something went wrong! Please contact admin.";

    }
 
    return json_decode($response, true);
}


function createOrder($accessToken, $order, $shopId = 16) {
    $createOrderApiUrl = API_URL."/shop/".$shopId."/order";

    $postdata = json_encode($order);

    $header = array(
        "Authorization: Bearer ".$accessToken, 
        'Content-Type: application/json',
        //'Content-Length: ' . strlen($postdata)
    );
 
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $createOrderApiUrl);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);

    $response = curl_exec($ch);
    $err = curl_error($ch);

    curl_close($ch);

    if ($err) {

        echo "cURL Error #01 (Order): " . $err;

    } else {

        $response = json_decode($response, true);
        if(array_key_exists("id", $response)) return $response;
        if(array_key_exists("error", $response)) echo $response["error_description"];
        echo "cURL Error #02 (Order): Something went wrong! Please contact admin.";

    }
 
    return json_decode($response, true);
}


?>